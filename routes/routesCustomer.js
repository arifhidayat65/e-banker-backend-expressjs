'use strict';

module.exports =function(app){
    var controller = require('../controller/customerController');
    
    app.route('/customers/list').get(controller.customers);
    app.route('/customers').post(controller.insertCustomer);
    app.route('/customers/:id').get(controller.getCustomerById);
    app.route('/customers').put(controller.updateCustomer);
    app.route('/customers/:id').delete(controller.del);
    app.route('/login').post(controller.login);
    app.route('/register').post(controller.registerData);
}