module.exports = function(app){
    var controller = require('../controller/controllerTransaction');

    app.route('/transactions/list').get(controller.transactions);
    app.route('/transactions/:id').get(controller.getTransactionById);
    app.route('/transactions').post(controller.insertTransaction);
    app.route('/transactions').put(controller.updateTransaction);
    app.route('/transactions/:id').delete(controller.del);
    app.route('/transactions/historyCreadit/:in').get(controller.historyCredit);
    app.route('/transactions/historyDebit/:out').get(controller.historyDebit);
    app.route('/transactions/historyLatest/:io').get(controller.historyLatest);
    
}