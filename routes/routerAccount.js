module.exports = function(app){
    var controller = require('../controller/controllerAccount');

    app.route('/accounts').post(controller.insertAccount);
    app.route('/accounts/list').get(controller.accounts);
    app.route('/accounts/:id').get(controller.getAccountById);
    app.route('/accountsbycust/:idcustomer').get(controller.getAccountByIdCustomer);
    app.route('/accounts').put(controller.updateAccount);
    app.route('/accounts/:id').delete(controller.deleteAccount);
    app.route('/accounts/:id/transaction').get(controller.getTransByAcc);
    app.route('/accounts/pin').post(controller.cekPinAccount);
    app.route('/accounts',(req, res)=>{
        let filter = {};
        filter.ballance = req.query.ballance;
        
        if (req.query.accountNumber) {
            filter.accountNumber = req.query.accountNumber;
        }
    
    })
        
}