const {
    Wallet
} = require('../db/con-sequelize');
var logger = require('winston');

exports.getById = function getById(id, callback) {
    Wallet.findById(id)
        .then((wallet) => {
            return callback(null, wallet);
        })
        .catach((error) => {
            logger.error(error);
            return callback(error);
        })
};

exports.getAll = function getAll(callback) {
    Wallet.findAll()
        .then((wallet) => {
            return callback(null, wallet);
        })
        .catach((error) => {
            logger.error(error);
            return callback(error);
        })
};
